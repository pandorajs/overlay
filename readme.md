#overlay

[![Build Status](https://api.travis-ci.org/pandorajs/overlay.png?branch=master)](http://travis-ci.org/pandorajs/overlay)
[![Coverage Status](https://coveralls.io/repos/pandorajs/overlay/badge.png?branch=master)](https://coveralls.io/r/pandorajs/overlay?branch=master)

 > overlay, provides positioning, seajs module

##how to demo

1. checkout
1. run `npm install`
1. run `spm install`
1. run `grunt`
1. view files in `/demo`

##how to use

1. run `spm install pandora/overlay`
1. write `require('pandora/overlay/VERSION.NUMBER/overlay')`

##find examples

1. view the source files in '/src'

##history

- 1.0.0 - release
